import com.google.gson.*;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.util.*;

import static io.restassured.RestAssured.given;

public class UserTest {


    @Test
    public void runTest() {

        Response response = addressTheService();

        JsonObject jsonObject = extractData(response);

        List<WeatherParams> list = populateWeatherParamsObj(jsonObject);

        calculate(list);
    }

    /**
     * Sending a get Rest assured request to weather service
     * The request contains the cites id
     * For two cities the response is empty so i didn't included them
     *
     * @return response
     */
    private Response addressTheService() {

        return given()
                .auth()
                .preemptive()
                .basic("igal.ep@no-spam.ws", "qwerty1234")
                .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
                .header("x-api-key", "2129b0d4a034345575dc2dca4e05a2e6")
                .when()
                .get("http://api.openweathermap.org/data/2.5/group?id=1880252,2193733,3833367,4164138,2643743,2950159,3413829,1283240&units=metric")
                .then().contentType(ContentType.JSON).extract().response();
    }

    /**
     * @param response
     * @return JsonObject
     * Extract the data from the response
     */
    private JsonObject extractData(Response response) {
        JsonElement jElement = new JsonParser().parse(response.asString());
        JsonObject jObject = jElement.getAsJsonObject();
        JsonArray jArray = jObject.getAsJsonArray("list");
        return jObject;
    }

    /**
     * @param jObject
     * @return List<WeatherParams>
     */
    private List<WeatherParams> populateWeatherParamsObj(JsonObject jObject) {
        WeatherParams weatherParams;
        List<WeatherParams> weatherParamsList = new ArrayList<>();
        JsonArray jArray = jObject.getAsJsonArray("list");

        for (int i = 0; i < jArray.size(); i++) {

            weatherParams = new WeatherParams();

            // Get the json at i
            jObject = jArray.get(i).getAsJsonObject();
            JsonObject main, sys;
            JsonElement temp, sunrise, sunset;

            // Get the main
            main = jObject.getAsJsonObject("main");
            // Get the temp
            temp = main.get("temp");

            // Get the sys
            sys = jObject.getAsJsonObject("sys");

            // Get the sunrise
            sunrise = sys.get("sunrise");

            // Get the sunset
            sunset = sys.get("sunset");

            Long dayLight = sunset.getAsLong() - sunrise.getAsInt();

            // Initialized weatherParams
            weatherParams.setTempt(temp.getAsDouble());
            weatherParams.setSunrise(sunrise.getAsLong());
            weatherParams.setSunset(sunset.getAsLong());
            weatherParams.setDayLight(dayLight);

            // Add weatherParams to list
            weatherParamsList.add(weatherParams);
        }
        return weatherParamsList;
    }

    /**
     *
     * @param weatherParamsList
     * Get the temperature for the longest and the shortest day light
     */
    private void calculate(List<WeatherParams> weatherParamsList) {

        WeatherParams minDayLight, maxDayLight;
        Double minTemp, maxTemp;

        // Get the min day light
        minDayLight =
                weatherParamsList
                        .stream()
                        .min(Comparator.comparing(WeatherParams::getDayLight))
                        .orElseThrow(NoSuchElementException::new);

        minTemp = minDayLight.getTempt();

        System.out.println("The temperature for the shortest day light is " + minTemp);

        // Get the max day light
        maxDayLight =
                weatherParamsList
                        .stream()
                        .max(Comparator.comparing(WeatherParams::getDayLight))
                        .orElseThrow(NoSuchElementException::new);

        maxTemp = maxDayLight.getTempt();

        System.out.println("The temperature for the longest day light is " + maxTemp);
    }
}


