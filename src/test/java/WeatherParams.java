import com.google.gson.JsonPrimitive;

public class WeatherParams {

    private Long sunrise;

    public Long getSunrise(){
        return this.sunrise;
    }
    void setSunrise(Long value){
        this.sunrise = value;
    }

    private Long sunset;

    public Long getSunset(){
        return this.sunset;
    }

    void setSunset(Long value){
        this.sunset = value;
    }

    private Double temp;

    public Double getTempt(){
        return this.temp;
    }
    void setTempt(Double value){

        this.temp = value;
    }

    private Long dayLight;

    Long getDayLight(){
        return this.dayLight;
    }
    void setDayLight(Long value){
        this.dayLight = value;
    }
}
